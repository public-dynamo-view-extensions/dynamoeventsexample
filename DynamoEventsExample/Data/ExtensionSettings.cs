﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamoEventsExample.Data
{
    public class ExtensionSettings
    {
        public bool ShouldLaunchAtStart { get; set; }
    }
}
