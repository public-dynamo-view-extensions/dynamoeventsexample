﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DynamoEventsExample.Data
{
    public static class CommonData
    {
        public static string DanimoDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Danimo");
        public static string DanimoSettingsDirectory = Path.Combine(DanimoDirectory, "Settings");
        public static string ExtensionSettingsFilepath = Path.Combine(DanimoSettingsDirectory, "EventWatcherSettings.json");
        public static ExtensionSettings EventWatcherSettings { get; set; } = null;        
    }
}
