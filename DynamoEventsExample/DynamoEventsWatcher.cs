﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Dynamo.Wpf.Extensions;

using DynamoEventsExample.Data;
using DynamoEventsExample.Utils;
using DynamoEventsExample.EventWatcher.ViewModels;
using DynamoEventsExample.EventWatcher.Views;

namespace DynamoEventsExample
{
    public class DynamoEventsWatcher : IViewExtension
    {

        #region Properties

        public string UniqueId
        {
            get
            {
                return Guid.NewGuid().ToString();
            }
        }

        public string Name
        {
            get
            {
                return "Dynamo Events Watcher";
            }
        }

        #endregion

        #region Methods

        public void Startup(ViewStartupParams p)
        {
            IOUtils.EnsureDirectoriesExists();
            IOUtils.LoadSettings();
        }

        public void Loaded(ViewLoadedParams p)
        {
            EventWatcherViewModel._viewLoadedParams = p;

            Menu mainMenu = p.dynamoMenu;

            MenuItem _eventMainMenu = new MenuItem
            {
                Header = "Dynamo Event Watcher"
            };
            mainMenu.Items.Add(_eventMainMenu);

            MenuItem _EventWatcher = new MenuItem
            {
                Header = "Launch Event Watcher",                
            };
            _eventMainMenu.Items.Add(_EventWatcher);

            _EventWatcher.Click += (sender, args) =>
            {
                if (EventWatcherViewModel.IsActive == false)
                {
                    EventWatcherViewModel ewVM = new EventWatcherViewModel();
                    var window = new EventWatcherView
                    {
                        MainDockPanel = { DataContext = ewVM },
                        Owner = p.DynamoWindow
                    };

                    window.Left = window.Owner.Left + 500;
                    window.Top = window.Owner.Top + 200;

                    window.Show();
                }
            };

            Separator sep = new Separator()
            {
                Width = 1                
            };
            _eventMainMenu.Items.Add(sep);

            MenuItem _openAtStart = new MenuItem()
            {
                Header = "Should Launch at start?",
                IsCheckable = true,
                IsChecked = CommonData.EventWatcherSettings.ShouldLaunchAtStart
            };
            _eventMainMenu.Items.Add(_openAtStart);

            _openAtStart.Click += (sender, args) =>
            {
                CommonData.EventWatcherSettings.ShouldLaunchAtStart = _openAtStart.IsChecked;
                IOUtils.SaveSettings();
            };

            if (CommonData.EventWatcherSettings.ShouldLaunchAtStart == true)
            {
                var ewVM = new EventWatcherViewModel();
                
                var window = new EventWatcherView
                {
                    MainDockPanel = { DataContext = ewVM },
                    Owner = p.DynamoWindow
                };

                window.Left = window.Owner.Left + 500;
                window.Top = window.Owner.Top + 200;

                window.Show();
            }
        }

        public void Dispose()
        {
            
        }

        public void Shutdown()
        {
            EventWatcherViewModel.IsActive = false;
        }

        #endregion
    }
}
