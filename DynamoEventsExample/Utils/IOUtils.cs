﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using DynamoEventsExample.Data;

namespace DynamoEventsExample.Utils
{
    public static class IOUtils
    {

        /// <summary>
        /// Ensure required Directories exist.
        /// </summary>
        public static void EnsureDirectoriesExists()
        {
            if(!Directory.Exists(CommonData.DanimoDirectory))
            {
                Directory.CreateDirectory(CommonData.DanimoDirectory);
                Directory.CreateDirectory(CommonData.DanimoSettingsDirectory);
            }

            if(!Directory.Exists(CommonData.DanimoSettingsDirectory))
            {
                Directory.CreateDirectory(CommonData.DanimoSettingsDirectory);
            }
        }

        /// <summary>
        /// Load Extension Settings from file.
        /// </summary>
        /// <returns></returns>
        public static ExtensionSettings LoadSettings()
        {
            if (!File.Exists(CommonData.ExtensionSettingsFilepath))
            {
                var settings = new ExtensionSettings()
                {
                    ShouldLaunchAtStart = true
                };

                CommonData.EventWatcherSettings = settings;
                SaveSettings();                
                return settings;
            }
            else
            {
                using (StreamReader sr = new StreamReader(CommonData.ExtensionSettingsFilepath))
                {
                    var data = sr.ReadToEnd();
                    var settings = JsonConvert.DeserializeObject<ExtensionSettings>(data);
                    CommonData.EventWatcherSettings = settings;
                    return settings;
                };
            }            
        }

        /// <summary>
        /// Save Extension Settings to file.
        /// </summary>
        public static void SaveSettings()
        {
            using (StreamWriter sw = new StreamWriter(CommonData.ExtensionSettingsFilepath))
            {
                var json = JsonConvert.SerializeObject(CommonData.EventWatcherSettings);
                sw.Write(json);
            }
        }
    }
}
