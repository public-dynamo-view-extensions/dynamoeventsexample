﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dynamo.ViewModels;
using Dynamo.Wpf.Extensions;

using DynamoEventsExample.EventWatcher.ViewModels;

namespace DynamoEventsExample.EventWatcher.Watchers
{
    class DynamoViewModelEventsWatcher
    {

        #region Properties

        private DynamoViewModel dynamoViewModel;        
        private EventWatcherViewModel _VM;

        #endregion

        #region Constructor

        public DynamoViewModelEventsWatcher(ViewLoadedParams p, EventWatcherViewModel vm)
        {

            _VM = vm;
            dynamoViewModel = (DynamoViewModel)p.DynamoWindow.DataContext;

            if (dynamoViewModel != null)
            {                
                SubscribeToEvents();
            }
        }

        #endregion

        #region Methods

        private void UpdateVMEventLog(string msg)
        {
            var dt = DateTime.Now.ToString("dd-MM-yy HH:mm:ss.fff - ");
            _VM.DynamoViewModelEventLog += "\n" + dt + "DynamoModelEvent : " + msg;
        }

        private void SubscribeToEvents()
        {
            dynamoViewModel.PropertyChanged += DynamoViewModel_PropertyChanged;
            dynamoViewModel.RequestAboutWindow += DynamoViewModel_RequestAboutWindow;
            dynamoViewModel.RequestClose += DynamoViewModel_RequestClose;
            dynamoViewModel.RequestManagePackagesDialog += DynamoViewModel_RequestManagePackagesDialog;
            dynamoViewModel.RequestPackageManagerSearchDialog += DynamoViewModel_RequestPackageManagerSearchDialog;
            dynamoViewModel.RequestPackagePathsDialog += DynamoViewModel_RequestPackagePathsDialog;
            dynamoViewModel.RequestPackagePublishDialog += DynamoViewModel_RequestPackagePublishDialog;
            dynamoViewModel.RequestSave3DImage += DynamoViewModel_RequestSave3DImage;
            dynamoViewModel.RequestSaveImage += DynamoViewModel_RequestSaveImage;
            dynamoViewModel.RequestScaleFactorDialog += DynamoViewModel_RequestScaleFactorDialog;
            dynamoViewModel.RequestShowHideGallery += DynamoViewModel_RequestShowHideGallery;
            dynamoViewModel.RequestUserSaveWorkflow += DynamoViewModel_RequestUserSaveWorkflow;
            dynamoViewModel.SidebarClosed += DynamoViewModel_SidebarClosed;            
        }

        private void UnsubscribeToEvents()
        {
            dynamoViewModel.PropertyChanged -= DynamoViewModel_PropertyChanged;
            dynamoViewModel.RequestAboutWindow -= DynamoViewModel_RequestAboutWindow;
            dynamoViewModel.RequestClose -= DynamoViewModel_RequestClose;
            dynamoViewModel.RequestManagePackagesDialog -= DynamoViewModel_RequestManagePackagesDialog;
            dynamoViewModel.RequestPackageManagerSearchDialog -= DynamoViewModel_RequestPackageManagerSearchDialog;
            dynamoViewModel.RequestPackagePathsDialog -= DynamoViewModel_RequestPackagePathsDialog;
            dynamoViewModel.RequestPackagePublishDialog -= DynamoViewModel_RequestPackagePublishDialog;
            dynamoViewModel.RequestSave3DImage -= DynamoViewModel_RequestSave3DImage;
            dynamoViewModel.RequestSaveImage -= DynamoViewModel_RequestSaveImage;
            dynamoViewModel.RequestScaleFactorDialog -= DynamoViewModel_RequestScaleFactorDialog;
            dynamoViewModel.RequestShowHideGallery -= DynamoViewModel_RequestShowHideGallery;
            dynamoViewModel.RequestUserSaveWorkflow -= DynamoViewModel_RequestUserSaveWorkflow;
            dynamoViewModel.SidebarClosed -= DynamoViewModel_SidebarClosed;
        }

        #endregion

        #region Event Handling

        private void DynamoViewModel_SidebarClosed(object sender, EventArgs e)
        {
            UpdateVMEventLog("Sidebar Closed");
        }

        private void DynamoViewModel_RequestUserSaveWorkflow(object sender, WorkspaceSaveEventArgs e)
        {
            UpdateVMEventLog("Request User Save Workflow");
        }

        private void DynamoViewModel_RequestShowHideGallery(bool showGallery)
        {
            UpdateVMEventLog("Request Show/Hide Gallery");
        }

        private void DynamoViewModel_RequestScaleFactorDialog(object sender, EventArgs e)
        {
            UpdateVMEventLog("Request Scale Factor Dialog");
        }

        private void DynamoViewModel_RequestSaveImage(object sender, ImageSaveEventArgs e)
        {
            UpdateVMEventLog("Request Save Image");
        }

        private void DynamoViewModel_RequestSave3DImage(object sender, ImageSaveEventArgs e)
        {
            UpdateVMEventLog("Request Save 3D Image");
        }

        private void DynamoViewModel_RequestPackagePublishDialog(Dynamo.PackageManager.PublishPackageViewModel publishViewModel)
        {
            UpdateVMEventLog("Request Package Publish Dialog");
        }

        private void DynamoViewModel_RequestPackagePathsDialog(object sender, EventArgs e)
        {
            UpdateVMEventLog("Request Package Paths Dialog");
        }

        private void DynamoViewModel_RequestPackageManagerSearchDialog(object sender, EventArgs e)
        {
            UpdateVMEventLog("Request Package Manager Search Dialog");
        }

        private void DynamoViewModel_RequestManagePackagesDialog(object sender, EventArgs e)
        {
            UpdateVMEventLog("Request Manage Packages Dialog");
        }

        private void DynamoViewModel_RequestClose(object sender, EventArgs e)
        {
            UpdateVMEventLog("Request Close");
        }

        private void DynamoViewModel_RequestAboutWindow(DynamoViewModel aboutViewModel)
        {
            UpdateVMEventLog("Request About Window");
        }

        private void DynamoViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateVMEventLog("Property Changed : " + e.PropertyName);
        }

        #endregion

    }
}
