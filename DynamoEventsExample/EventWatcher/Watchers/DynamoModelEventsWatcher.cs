﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dynamo.Wpf.Extensions;
using Dynamo.Models;
using Dynamo.ViewModels;
using Dynamo.Graph.Workspaces;

using DynamoEventsExample.EventWatcher.ViewModels;

namespace DynamoEventsExample.EventWatcher.Watchers
{
    class DynamoModelEventsWatcher
    {

        #region Properties

        private DynamoViewModel _dvm;
        private DynamoModel dynamoModel;
        private EventWatcherViewModel _VM;

        #endregion

        #region Constructor

        public DynamoModelEventsWatcher(ViewLoadedParams p, EventWatcherViewModel vm)
        {

            _VM = vm;            
            _dvm = (DynamoViewModel)p.DynamoWindow.DataContext;            

            if (_dvm != null)
            {
                dynamoModel = _dvm.Model;

                SubscribeToEvents();
            }
        }

        #endregion

        #region Methods

        private void UpdateVMEventLog(string msg)
        {
            var dt = DateTime.Now.ToString("dd-MM-yy HH:mm:ss.fff - ");
            _VM.DynamoModelEventLog += "\n" + dt + "DynamoModelEvent : " + msg;
        }

        private void SubscribeToEvents()
        {
            dynamoModel.PropertyChanged += dynamoModel_PropertyChanged;
            dynamoModel.CleaningUp += dynamoModel_CleaningUp;
            dynamoModel.DeletionComplete += DynamoModel_DeletionComplete;
            dynamoModel.DeletionStarted += DynamoModel_DeletionStarted;
            dynamoModel.DynamoReady += DynamoModel_DynamoReady;            
            dynamoModel.EvaluationCompleted += DynamoModel_EvaluationCompleted;
            dynamoModel.RefreshCompleted += DynamoModel_RefreshCompleted;
            dynamoModel.RequestCancelActiveStateForNode += DynamoModel_RequestCancelActiveStateForNode;
            dynamoModel.RequestLayoutUpdate += DynamoModel_RequestLayoutUpdate;
            dynamoModel.RequestNodeSelect += DynamoModel_RequestNodeSelect;
            dynamoModel.RequestsCrashPrompt += DynamoModel_RequestsCrashPrompt;
            dynamoModel.RequestsRedraw += DynamoModel_RequestsRedraw;
            dynamoModel.RequestWorkspaceBackUpSave += DynamoModel_RequestWorkspaceBackUpSave;            
            dynamoModel.RunCompleted += DynamoModel_RunCompleted;
            dynamoModel.ShutdownCompleted += DynamoModel_ShutdownCompleted;
            dynamoModel.ShutdownStarted += DynamoModel_ShutdownStarted;
            dynamoModel.WorkspaceAdded += DynamoModel_WorkspaceAdded;
            dynamoModel.WorkspaceCleared += DynamoModel_WorkspaceCleared;
            dynamoModel.WorkspaceClearing += DynamoModel_WorkspaceClearing;
            dynamoModel.WorkspaceHidden += DynamoModel_WorkspaceHidden;
            dynamoModel.WorkspaceOpening += DynamoModel_WorkspaceOpening;
            dynamoModel.WorkspaceRemoved += DynamoModel_WorkspaceRemoved;
            dynamoModel.WorkspaceRemoveStarted += DynamoModel_WorkspaceRemoveStarted;
            dynamoModel.WorkspaceSaved += DynamoModel_WorkspaceSaved;            
        }

        private void UnsubscribeToEvents()
        {
            dynamoModel.PropertyChanged -= dynamoModel_PropertyChanged;
            dynamoModel.CleaningUp -= dynamoModel_CleaningUp;
            dynamoModel.DeletionComplete -= DynamoModel_DeletionComplete;
            dynamoModel.DeletionStarted -= DynamoModel_DeletionStarted;
            dynamoModel.DynamoReady -= DynamoModel_DynamoReady;
            dynamoModel.EvaluationCompleted -= DynamoModel_EvaluationCompleted;
            dynamoModel.RefreshCompleted -= DynamoModel_RefreshCompleted;
            dynamoModel.RequestCancelActiveStateForNode -= DynamoModel_RequestCancelActiveStateForNode;
            dynamoModel.RequestLayoutUpdate -= DynamoModel_RequestLayoutUpdate;
            dynamoModel.RequestNodeSelect -= DynamoModel_RequestNodeSelect;
            dynamoModel.RequestsCrashPrompt -= DynamoModel_RequestsCrashPrompt;
            dynamoModel.RequestsRedraw -= DynamoModel_RequestsRedraw;
            dynamoModel.RequestWorkspaceBackUpSave -= DynamoModel_RequestWorkspaceBackUpSave;
            dynamoModel.RunCompleted -= DynamoModel_RunCompleted;
            dynamoModel.ShutdownCompleted -= DynamoModel_ShutdownCompleted;
            dynamoModel.ShutdownStarted -= DynamoModel_ShutdownStarted;
            dynamoModel.WorkspaceAdded -= DynamoModel_WorkspaceAdded;
            dynamoModel.WorkspaceCleared -= DynamoModel_WorkspaceCleared;
            dynamoModel.WorkspaceClearing -= DynamoModel_WorkspaceClearing;
            dynamoModel.WorkspaceHidden -= DynamoModel_WorkspaceHidden;
            dynamoModel.WorkspaceOpening -= DynamoModel_WorkspaceOpening;
            dynamoModel.WorkspaceRemoved -= DynamoModel_WorkspaceRemoved;
            dynamoModel.WorkspaceRemoveStarted -= DynamoModel_WorkspaceRemoveStarted;
            dynamoModel.WorkspaceSaved -= DynamoModel_WorkspaceSaved;
        }

        #endregion

        #region Event Handling

        private void DynamoModel_WorkspaceSaved(WorkspaceModel workspace)
        {
            UpdateVMEventLog("Workspace Saved");
        }

        private void DynamoModel_WorkspaceRemoveStarted(WorkspaceModel obj)
        {
            UpdateVMEventLog("Workspace Removal Started");
        }

        private void DynamoModel_WorkspaceRemoved(WorkspaceModel obj)
        {
            UpdateVMEventLog("Workspace Renmoved");
        }

        private void DynamoModel_WorkspaceOpening(object obj)
        {
            UpdateVMEventLog("Workspace Opening");
        }

        private void DynamoModel_WorkspaceHidden(WorkspaceModel workspace)
        {
            UpdateVMEventLog("Workspace Hidden");
        }

        private void DynamoModel_WorkspaceClearing()
        {
            UpdateVMEventLog("Workspace Clearing");
        }

        private void DynamoModel_WorkspaceCleared(WorkspaceModel obj)
        {
            UpdateVMEventLog("Workspace Cleared");
        }

        private void DynamoModel_WorkspaceAdded(WorkspaceModel obj)
        {
            UpdateVMEventLog("Workspace Added");
        }

        private void DynamoModel_ShutdownStarted(DynamoModel model)
        {
            UpdateVMEventLog("Shutdown Started");
        }

        private void DynamoModel_ShutdownCompleted(DynamoModel model)
        {
            UpdateVMEventLog("Shutdown Complete");
        }

        private void DynamoModel_RunCompleted(object sender, bool success)
        {
            UpdateVMEventLog("Run Completed");
        }

        private void DynamoModel_RequestWorkspaceBackUpSave(string arg1, bool arg2)
        {
            UpdateVMEventLog("Request Workspace Backup Save");
        }

        private void DynamoModel_RequestsRedraw(object sender, EventArgs e)
        {
            UpdateVMEventLog("Request Redraw");
        }

        private void DynamoModel_RequestsCrashPrompt(object sender, Dynamo.Core.CrashPromptArgs e)
        {
            UpdateVMEventLog("Request Crash Prompt");
        }

        private void DynamoModel_RequestNodeSelect(object sender, EventArgs e)
        {
            UpdateVMEventLog("Request Node Select");
        }

        private void DynamoModel_RequestLayoutUpdate(object sender, EventArgs e)
        {
            UpdateVMEventLog("Request Layout Update");
        }

        private void DynamoModel_RequestCancelActiveStateForNode(Dynamo.Graph.Nodes.NodeModel node)
        {
            UpdateVMEventLog("Request Cancel Active State For Node");
        }

        private void DynamoModel_RefreshCompleted(HomeWorkspaceModel obj)
        {
            UpdateVMEventLog("Refresh Completed");
        }

        private void DynamoModel_EvaluationCompleted(object sender, EvaluationCompletedEventArgs e)
        {
            UpdateVMEventLog("Workspace Evaluation Completed");
        }

        private void DynamoModel_DynamoReady(Dynamo.Extensions.ReadyParams obj)
        {
            UpdateVMEventLog("Dynamo Ready");
        }

        private void DynamoModel_DeletionStarted()
        {
            UpdateVMEventLog("Deletion Started");
        }

        private void DynamoModel_DeletionComplete(object sender, EventArgs e)
        {
            UpdateVMEventLog("Deletion Completed");
        }

        private void dynamoModel_CleaningUp()
        {
            UpdateVMEventLog("Cleaning Up");
        }

        private void dynamoModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            UpdateVMEventLog("Property Changed : " + e.PropertyName);
        }

        #endregion
    }
}
