﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using Dynamo.Wpf.Extensions;

using DynamoEventsExample;
using DynamoEventsExample.EventWatcher.Watchers;

namespace DynamoEventsExample.EventWatcher.ViewModels
{
    class EventWatcherViewModel : INotifyPropertyChanged
    {

        #region Properties

        public static bool IsActive = false;

        private static int index = 0;

        public static ViewLoadedParams _viewLoadedParams { get; set; }

        private WorkspaceEventsWatcher _wsEvents;
        private DynamoModelEventsWatcher _dmEvents;
        private DynamoViewModelEventsWatcher _dvmEvents;

        private string _visualLog;

        public string VisualLog
        {
            get { return _visualLog; }
            set
            {
                if (!string.IsNullOrEmpty(value) && value != _visualLog)
                {
                    _visualLog = value;
                    OnPropertyChanged("VisualLog");
                }                
            }
        }


        private string _WorkspaceEventLog;
        public string WorkspaceEventLog
        {
            get { return _WorkspaceEventLog; }
            set
            {
                if (!string.IsNullOrEmpty(value) && value != _WorkspaceEventLog)
                {
                    _WorkspaceEventLog = value;
                    OnPropertyChanged("WorkspaceEventLog");
                    if (index == 0)
                    {
                        VisualLog = _WorkspaceEventLog;
                    }
                }
            }
        }

        private string _dynamoModelEventLog;
        public string DynamoModelEventLog
        {
            get { return _dynamoModelEventLog; }
            set
            {
                if (!string.IsNullOrEmpty(value) && value != _dynamoModelEventLog)
                {
                    _dynamoModelEventLog = value;                    
                    OnPropertyChanged("DynamoModelEventLog");
                    if (index == 1)
                    {
                        VisualLog = _dynamoModelEventLog;
                    }
                }                
            }
        }

        private string _dynamoViewModelEventLog;
        public string DynamoViewModelEventLog
        {
            get { return _dynamoViewModelEventLog; }
            set
            {
                if (!string.IsNullOrEmpty(value) && value != _dynamoViewModelEventLog)
                {
                    _dynamoViewModelEventLog = value;
                    OnPropertyChanged("DynamoViewModelEventLog");
                    if (index == 2)
                    {
                        VisualLog = _dynamoViewModelEventLog;
                    }
                }
            }
        }


        #endregion

        #region Constructor

        /// <summary>
        /// Constructs a new static instance of the Event Watcher ViewModel.
        /// </summary>
        public EventWatcherViewModel()
        {
            IsActive = true;
            ClearAllLogs();

            if (DesignerProperties.GetIsInDesignMode(new DependencyObject())) return;
            _wsEvents = new WorkspaceEventsWatcher(_viewLoadedParams, this);
            _dmEvents = new DynamoModelEventsWatcher(_viewLoadedParams, this);
            _dvmEvents = new DynamoViewModelEventsWatcher(_viewLoadedParams, this);

            ClearLogsCommand = new RelayCommand(OnClearLog);
            ClearAllLogsCommand = new RelayCommand(OnClearAllLogs);
            ShowWorkspaceLogsCommand = new RelayCommand(OnShowWorkspaceLogs);
            ShowDynamoModelLogsCommand = new RelayCommand(OnShowDynamoModelLogs);
            ShowDynamoViewModelLogsCommand = new RelayCommand(OnShowDynamoViewModelLogs);           
        }

        #endregion

        #region Methods

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        private void ClearLogs()
        {
            if (index == 0)
            {
                WorkspaceEventLog = "Workspace Events:\n";
            }            
            if (index == 1)
            {
                DynamoModelEventLog = "DynamoModel Events:\n";
            }
            if (index == 2)
            {
                DynamoViewModelEventLog = "DynamoViewModel Events:\n";
            }

        }


        private void ClearAllLogs()
        {
            WorkspaceEventLog = "Workspace Events:\n";
            DynamoModelEventLog = "DynamoModel Events:\n";
            DynamoViewModelEventLog = "DynamoViewModel Events:\n";
        }

        private void OnClearLog()
        {
            if (CanClearLog())
            {
                ClearLogs();
            }            
        }

        public bool CanClearLog()
        {
            return true;
        }

        private void OnClearAllLogs()
        {
            if (CanClearAllLogs())
            {
                ClearAllLogs();
            }
        }

        public bool CanClearAllLogs()
        {
            return true;
        }

        private void OnShowWorkspaceLogs()
        {
            if (CanShowWorkspaceLogs())
            {
                index = 0;
                VisualLog = WorkspaceEventLog;
            }
        }

        public bool CanShowWorkspaceLogs()
        {
            return true;
        }

        private void OnShowDynamoModelLogs()
        {
            if (CanShowDynamoModelLogs())
            {
                index = 1;
                VisualLog = DynamoModelEventLog;
            }
        }

        public bool CanShowDynamoModelLogs()
        {
            return true;
        }

        private void OnShowDynamoViewModelLogs()
        {
            if (CanShowDynamoViewModelLogs())
            {
                index = 2;
                VisualLog = DynamoViewModelEventLog;
            }
        }

        public bool CanShowDynamoViewModelLogs()
        {
            return true;
        }

        #endregion

        #region Commands

        public RelayCommand ClearLogsCommand { get; private set; }
        public RelayCommand ClearAllLogsCommand { get; private set; }
        public RelayCommand ShowWorkspaceLogsCommand { get; private set; }
        public RelayCommand ShowDynamoModelLogsCommand { get; private set; }
        public RelayCommand ShowDynamoViewModelLogsCommand { get; private set; }

        #endregion

    }

}
